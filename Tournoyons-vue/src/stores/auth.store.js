import { defineStore } from 'pinia';
import router from '@/router';
import { apiLogin, apiRefresh } from '@/api/api.js';
import jwt_decode from 'jwt-decode'

export const useAuthStore = defineStore({
    id: 'auth',
    state: () => ({
        tokens: JSON.parse(localStorage.getItem('tokens')),
        user: null,
        returnUrl: null
    }),
    actions: {
        async login(username, password) {
            try {
                const tokens = await apiLogin(username, password);    
                if (tokens) {
                    this.tokens = tokens;
                    localStorage.setItem('tokens', JSON.stringify(this.tokens));
                    router.go(this.returnUrl || '/');
                }
            } catch (error) {
                console.log("Login error")
                console.log(error)
            }
        },
        logout() {
            this.tokens = null;
            this.user = null;
            localStorage.removeItem('tokens');
            router.push('/login');
        },
        isLogged() {
            if (this.tokens != null) {
                const access_exp = jwt_decode(this.tokens.access).exp
                if (!(access_exp - (Date.now()/1000) > 10)) {
                    return false
                }
                return true;
            }
            return false;
        },
        async inspectToken(){
            if(this.isLogged()){
              const access_exp = jwt_decode(this.tokens.access).exp
              const refresh_exp = jwt_decode(this.tokens.refresh).exp

              // Si l'access est valide => OK
              if (!(access_exp - (Date.now()/1000) > 10)) {
                // Sinon, on regarde si le refresh est toujours valide
                if (refresh_exp - (Date.now()/1000) > 10) {
                    const new_token = await apiRefresh()
                    this.tokens.access = new_token.access
                    localStorage.setItem('tokens', JSON.stringify(this.tokens));
                }
                else {
                    this.logout();
                }
              }
            }
        }
    }
});