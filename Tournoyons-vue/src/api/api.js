import { APISettings } from './config.js'
import { useAuthStore } from "@/stores/auth.store.js";

async function apiLogin(username, password) {
  const res = await fetch(APISettings.baseURL + '/token/', {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ username, password }),
  });

  if (res.ok) {
    const finalRes = await res.json()
    return finalRes;
  }

  return null;
}

async function apiRegister(username, password, password2, email) {
  const res = await fetch(APISettings.baseURL + '/user/', {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ username, password, password2, email }),
  });

  if (res.ok) {
    const finalRes = await res.json();
    return 1;
  }

  return 0;
}

async function apiRefresh() {
  const authStore = useAuthStore();
  const refresh = authStore.tokens.refresh;
  const res = await fetch(APISettings.baseURL + '/token/refresh/', {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ refresh }),
  });

  if (res.ok) {
    const finalRes = await res.json();
    return finalRes;
  }

  return null;
}

async function getTournaments () {
  const res = await fetch(APISettings.baseURL + '/tournament/', {
        method: 'GET',
        headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes.results;
}

async function createTournament(name, description, nb_player_by_round, nb_player_max) {
  const res = await fetch(APISettings.baseURL + '/tournament/', {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ 
      "name": name,
      "description": description,
      "nb_player_by_round": nb_player_by_round,
      "nb_max_player": nb_player_max
    }),
  });

  if (res.ok) {
    const finalRes = await res.json();
    return finalRes.id;
  }

  return 0;
}

async function createStep(tournament_id, step_type) {
  const res = await fetch(APISettings.baseURL + '/tournament/' + tournament_id + '/step/', {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({
      "step_type": step_type
    }),
  });

  if (res.ok) {
    const finalRes = await res.json();
    return finalRes.id;
  }

  return 0;
}

async function getTournament (id) {
  const res = await fetch(APISettings.baseURL + '/tournament/' + id + '/', {
        method: 'GET',
        headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes;
}

async function getPlayers (id) {
  const res = await fetch(APISettings.baseURL + '/tournament/' + id + '/player/', {
        method: 'GET',
        headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes.results;
}

async function getSteps (tournament_id) {
  const res = await fetch(APISettings.baseURL + '/tournament/' + tournament_id + '/step/', {
        method: 'GET',
        headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes.results;
}

async function getStep (tournament_id, step_id) {
  const res = await fetch(APISettings.baseURL + '/tournament/' + tournament_id + '/step/' + step_id + '/', {
        method: 'GET',
        headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes;
}

async function getUser() {
  const res = await fetch(APISettings.baseURL + '/user/current_user/', {
      method: 'GET',
      headers: getHeaders(),
  });
  const finalRes = await res.json();
  return finalRes;
}

function getHeaders() {
  const authStore = useAuthStore();
  const isLoggedIn = !!authStore?.tokens;
  const headers = {
      "Accept": "application/json",
      "Content-Type": "application/json"
  };

  if (isLoggedIn) {
      headers['Authorization'] = 'Bearer ' + authStore.tokens.access
  }

  return headers;
}

export {
    getTournaments,
    getTournament,
    getPlayers,
    getUser,
    apiLogin,
    apiRefresh,
    apiRegister,
    createTournament,
    createStep,
    getSteps,
    getStep
}