import { createRouter, createWebHashHistory } from 'vue-router'

import Tournament from '@/components/tournament/Tournament2.vue'
import TournamentDetail from '@/components/tournament/TournamentDetail.vue'
import TournamentCreate from '@/components/tournament/TournamentCreate.vue'
import TournamentTemplate from '@/components/tournament/TournamentTemplate.vue'
import PlayerTable from '@/components/player/PlayerTable.vue'
import StepTable from '@/components/step/StepTable.vue'
import StepCreate from '@/components/step/StepCreate.vue'
import StepDetail from '@/components/step/StepDetail.vue'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
import Home from '@/components/Home.vue'
import HomeTemplate from '@/components/HomeTemplate.vue'
import { useAuthStore } from '@/stores/auth.store.js';
import { APISettings } from '@/api/config.js'


const routes = [
  { path: '/login', component: Login},
  { path: '/register', component: Register},
  { path: '/', component: HomeTemplate, children : [
    { path: '', component: Home },
    { path: 'tournament', component: Tournament },
    { path: 'tournament/create', component: TournamentCreate },
  ]},
  { path: '/tournament/:id', component: TournamentTemplate, children: [
    { path: 'resume', component: TournamentDetail },
    { path: 'player', component: PlayerTable },
    { path: 'step', component: StepTable },
    { path: 'step/create', component: StepCreate },
    { path: 'step/:step_id', component: StepDetail },
  ]}
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach(async (to) => {
    // clear alert on route change
    // const alertStore = useAlertStore();
    // alertStore.clear();

  // redirect to login page if not logged in and trying to access a restricted page 
  const authRequired = !APISettings.publicPages.includes(to.path);
  const authStore = useAuthStore();

  if (authRequired && !authStore.isLogged()) {
      authStore.returnUrl = to.fullPath;
      return '/login';
  } else if (!authRequired && authStore.isLogged()) {
      return '/'
  }
});

export default router
