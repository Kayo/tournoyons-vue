import { createApp } from 'vue'
import router from '@/router'

import vuetify from './plugins/vuetify'
import { loadFonts } from './plugins/webfontloader'

import App from './App.vue'
import { createPinia } from "pinia";

loadFonts()

createApp(App)
  .use(vuetify)
  .use(createPinia())
  .use(router)
  .mount('#app')